import requests
import json
import datetime

# return latest URL and date of the RSVP
def update_url_date(rsvp_data,rsvp_date_farthest,rsvp_url_farthest):
	rsvp_date_farthest = rsvp_data['event']['time']
	rsvp_url_farthest = rsvp_data['event']['event_url']
	return rsvp_date_farthest,rsvp_url_farthest

# return each place value
def update_top_three(rsvp_country_top_three,rsvp_country_all,num):
	# if num = 1 then it would look for most RSVP country by looking at the last index in the sorted list
	# if num = 2 then it would look for most RSVP country by looking at the second to last index in the sorted list
	# if num = 3 then it would look for most RSVP country by looking at the third to last index in the sorted list
	return sorted(rsvp_country_all.keys())[-num],sorted(rsvp_country_all.values())[-num]

# printing the aggregate calculated information in a comma-delimited format. 
def metrics(rsvp_total_num,rsvp_date_farthest,rsvp_url_farthest,rsvp_country_all,rsvp_country_top_three,country):
	# declaring variables for readablity
	third_most_rsvp_country_name = rsvp_country_top_three['third']['Country Name']
	second_most_rsvp_country_name = rsvp_country_top_three['second']['Country Name']
	first_most_rsvp_country_name = rsvp_country_top_three['first']['Country Name']
	third_most_rsvp_country_num = rsvp_country_top_three['third']['num of rsvp']
	second_most_rsvp_country_num = rsvp_country_top_three['second']['num of rsvp']
	first_most_rsvp_country_num = rsvp_country_top_three['first']['num of rsvp']
	convert_milisecond = rsvp_date_farthest / 1000.0
	convert_rsvp_time = datetime.datetime.fromtimestamp(convert_milisecond).strftime('%Y-%m-%d %H:%M')
	print str(rsvp_total_num) + "," + str(convert_rsvp_time) + "," + rsvp_url_farthest + "," +first_most_rsvp_country_name + "," + str(first_most_rsvp_country_num) + "," + second_most_rsvp_country_name + "," + str(second_most_rsvp_country_num) + "," + str(third_most_rsvp_country_name) + "," + str(third_most_rsvp_country_num)

# main function
def main():

	# declaring and initialzing values for storing aggregate information
	rsvp_total_num = 0
	rsvp_date_farthest = 0
	rsvp_url_farthest = ''
	rsvp_country_top_three = {'third': {'Country Name': '', 'num of rsvp': 0}, 
							 'second': {'Country Name': '', 'num of rsvp': 0},
							 'first': {'Country Name': '', 'num of rsvp': 0}}
	rsvp_country_all = {}

	# making a get request to the URL
	url_request = requests.get('http://stream.meetup.com/2/rsvps', stream=True)

	# iterating over the response data 
	for raw_data in url_request.iter_lines():

		# if data found in one stream then encode raw data to JSON format
		if raw_data:
			rsvp_data = json.loads(raw_data)

			# declaring variable for readability
			rsvp_country = rsvp_data['group']['group_country']

			# incrementing total # of RSVPs received
			rsvp_total_num = rsvp_total_num + 1

			#if country name not found in dictionary then initialize value
			if rsvp_country not in rsvp_country_all:
				rsvp_country_all[rsvp_country] = 1
				rsvp_country_all.update({rsvp_country: ++int(rsvp_country_all.get(rsvp_country))})

			#otherwise, if country names exist then increment
			else:
				total_country_rvsp = int(rsvp_country_all.get(rsvp_country)) + 1
				rsvp_country_all.update({rsvp_country: total_country_rvsp})

			# update farthest RSVP TIME
			if rsvp_data['event']['time'] > rsvp_date_farthest:
				rsvp_date_farthest,rsvp_url_farthest = update_url_date(rsvp_data,rsvp_date_farthest,rsvp_url_farthest)				

			# if there only 1 country RSVP then automatically in first place
			if len(rsvp_country_all) == 1:
					rsvp_country_top_three['first']['Country Name'],rsvp_country_top_three['first']['num of rsvp'] = update_top_three(rsvp_country_top_three,rsvp_country_all,1)

			# if there only 2 countries RSVP then first and seond place should be occupied
			if len(rsvp_country_all) == 2:

				# if current country has less RSVP than the other country the place in second
				if int(rsvp_country_all.get(rsvp_country)) > rsvp_country_top_three['second']['num of rsvp']:

					# otherwise place it in first
					if int(rsvp_country_all.get(rsvp_country)) > rsvp_country_top_three['first']['num of rsvp']:
						rsvp_country_top_three['first']['Country Name'],rsvp_country_top_three['first']['num of rsvp'] = update_top_three(rsvp_country_top_three,rsvp_country_all,1)
						rsvp_country_top_three['second']['Country Name'],rsvp_country_top_three['second']['num of rsvp'] = update_top_three(rsvp_country_top_three,rsvp_country_all,2)
					
					# also needs to update top two placement if current country RSVP is lowest of the two
					else:
						rsvp_country_top_three['first']['Country Name'],rsvp_country_top_three['first']['num of rsvp'] = update_top_three(rsvp_country_top_three,rsvp_country_all,1)
						rsvp_country_top_three['second']['Country Name'],rsvp_country_top_three['second']['num of rsvp'] = update_top_three(rsvp_country_top_three,rsvp_country_all,2)

			# if more than 3 countries then all three placement should be used
			if len(rsvp_country_all) >= 3:

				# if current country has less RSVP than the top two countries the place in third
				if int(rsvp_country_all.get(rsvp_country)) > rsvp_country_top_three['third']['num of rsvp']:

					# if current country RSVP value is between first and third most countrie's RSVP 
					if int(rsvp_country_all.get(rsvp_country)) > rsvp_country_top_three['second']['num of rsvp']:

						# otherwise place it in first
						if int(rsvp_country_all.get(rsvp_country)) > rsvp_country_top_three['first']['num of rsvp']:
							rsvp_country_top_three['first']['Country Name'],rsvp_country_top_three['first']['num of rsvp'] = update_top_three(rsvp_country_top_three,rsvp_country_all,1)
							rsvp_country_top_three['second']['Country Name'],rsvp_country_top_three['second']['num of rsvp'] = update_top_three(rsvp_country_top_three,rsvp_country_all,2)
							rsvp_country_top_three['third']['Country Name'],rsvp_country_top_three['third']['num of rsvp'] = update_top_three(rsvp_country_top_three,rsvp_country_all,3)

						# also needs to update top two placement if current country RSVP is second lowest of the three
						else:
							rsvp_country_top_three['first']['Country Name'],rsvp_country_top_three['first']['num of rsvp'] = update_top_three(rsvp_country_top_three,rsvp_country_all,1)
							rsvp_country_top_three['second']['Country Name'],rsvp_country_top_three['second']['num of rsvp'] = update_top_three(rsvp_country_top_three,rsvp_country_all,2)
							rsvp_country_top_three['third']['Country Name'],rsvp_country_top_three['third']['num of rsvp'] = update_top_three(rsvp_country_top_three,rsvp_country_all,3)

					# also needs to update top two placement if current country RSVP is lowest of the three									
					else:		
						rsvp_country_top_three['first']['Country Name'],rsvp_country_top_three['first']['num of rsvp'] = update_top_three(rsvp_country_top_three,rsvp_country_all,1)
						rsvp_country_top_three['second']['Country Name'],rsvp_country_top_three['second']['num of rsvp'] = update_top_three(rsvp_country_top_three,rsvp_country_all,2)
						rsvp_country_top_three['third']['Country Name'],rsvp_country_top_three['third']['num of rsvp'] = update_top_three(rsvp_country_top_three,rsvp_country_all,3)										

			# printing the aggregate calculated information in a comma-delimited format.
			metrics(rsvp_total_num,rsvp_date_farthest,rsvp_url_farthest,rsvp_country_all,rsvp_country_top_three,rsvp_country)

# calling main function
if __name__ == "__main__":
    main()